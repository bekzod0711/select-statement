SELECT
  f.film_id,
  f.title AS movie_title,
  (
    SELECT
      CASE
        WHEN film.rating = 'PG-13' THEN '13+'
        WHEN film.rating = 'NC-17' THEN '18+'
        WHEN film.rating = 'R' THEN '17+'
        WHEN film.rating = 'PG' THEN '7+'
        WHEN film.rating = 'G' THEN 'All ages'
        ELSE 'Not specified'
      END
    FROM film
    WHERE film.film_id = f.film_id
  ) AS appropriate_age_rating
FROM
  film f
WHERE f.film_id IN (
  SELECT
    f.film_id
  FROM
    film f
    JOIN inventory i ON f.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    JOIN payment p ON r.rental_id = p.rental_id
  GROUP BY
    f.film_id
  ORDER BY
    COUNT(p.payment_id) DESC
  LIMIT 5
)
ORDER BY
  f.film_id;